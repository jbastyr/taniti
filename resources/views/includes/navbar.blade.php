<header>
    <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand ml-auto" href="{{ route('home') }}">
            {{ config('app.name', 'Taniti Tourism') }}
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            
        <li class="nav-item">
            <a class="nav-link" href="{{ route('attractions') }}">Attractions</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('transportation') }}">Transportation</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('lodging') }}">Lodging</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('about') }}">About</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('faq') }}">FAQ</a>
        </li>

        </ul>
    </div>
</nav>
</header>
