<div class="card">
    <div class="card-header" id="{{$name}}parent">
        <h5 class="mb-0 text-center">
        <a href="{{$link}}">{{$name}}</a>
        </h5>
    </div>

    <div class="card-body text-center">
        <img src="/img{{$imagename}}" alt="card image for {{$name}}" height="200px" width="200px">
    </div>

    <div class="card-footer">
        <p>
            {{$description}}
        </p>
    </div>
</div>