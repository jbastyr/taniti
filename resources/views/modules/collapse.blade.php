<div class="card">
    <div class="card-header" id="{{$name}}parent">
        <h5 class="mb-0 text-center">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#{{$name}}">
            {{$question}}
        </button>
        </h5>
    </div>

    <div id="{{$name}}" class="collapse" data-parent="#{{$name}}parent">
        <div class="card-body text-center">
        {{$answer}}
        </div>
    </div>
</div>