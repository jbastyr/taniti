
<div class="row">
    <div class="col-md-auto">
        <img src="/img{{$imagename}}" alt="info image for {{$name}}" height="300px" width="300px">
    </div>
    <div class="col">
        <h4>{{$name}}</h4>
        <p>{{$description}}</p>
    </div>
</div>
    