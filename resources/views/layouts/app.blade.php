<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token in all requests-->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Taniti Tourism') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Scripts at top to prevent flash of unstyled content -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')
</head>
<body class="pt-4">
    
    <div id="app">
        @include('includes.navbar')
        @yield('content')
    </div>
</body>
</html>
