@extends('layouts.app')
@section('content')
@include('includes.heading', ['imagename' => 'lodging.jpg', 'title' => 'Lodging Choices'])
    <div class="text-center">
        <h2>Lodging available for your stay</h2>
    </div>

    <div class="container">
        @foreach($info as $infoitem)
            <div class="row">
                <div class="col mt-4">
                    @include('modules.info', ['name' => $infoitem->getName(), 'description' => $infoitem->getDescription(), 'imagename' => $infoitem->getImagename()])
                </div>
            </div>
        @endforeach
    </div>
@endsection