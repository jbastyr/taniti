@extends('layouts.app')
@section('content')
@include('includes.heading', ['imagename' => 'home.jpg', 'title' => 'Taniti Tourism'])

<div class="text-center">
    <h2>Welcome to the Taniti Tourism website</h2>
</div>

<div class="container">
    <div class="row">
        @foreach($cards as $card)
            <div class="col mt-4">
                @include('modules.card', ['name' => $card->getName(), 'description' => $card->getDescription(), 'imagename' => $card->getImagename(), 'link' => $card->getLink()])
            </div>
        @endforeach
    </div>
</div>
@endsection
