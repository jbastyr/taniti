@extends('layouts.app')
@section('content')
@include('includes.heading', ['imagename' => 'faq.jpg', 'title' => 'Frequently Asked Questions'])
    <div class="accordian" id="accordian">
        @foreach ($faqs as $faq)
            @include( 'modules.collapse', ['name' => $faq->getName(), 'question' => $faq->getQuestion(), 'answer' => $faq->getAnswer()] )
        @endforeach
    </div>
    
@endsection