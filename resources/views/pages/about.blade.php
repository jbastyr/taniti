@extends('layouts.app')
@section('content')
@include('includes.heading', ['imagename' => 'about.jpg', 'title' => 'About / Contact'])
    <div class="text-center">
        <h2>Information about the island and contact info</h2>
    </div>

    <div class="container">
        @foreach($abouts as $about)
            @include('modules.about', ['name' => $about->getName(), 'description' => $about->getDescription()])
        @endforeach
    </div>
@endsection