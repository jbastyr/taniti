<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(){
        $cards = [
            new cardData(
                'Attractions',
                'Information on local attractions of interest',
                '/attractions/card.jpg',
                route('attractions')
            ),
            new cardData(
                'Transportation',
                'Different options for getting around the island',
                '/transportation/card.jpg',
                route('transportation')
            ),
            new cardData(
                'Lodging',
                'Choose from the many housing availabilities to suit your visit',
                '/lodging/card.jpg',
                route('lodging')
            ),
            new cardData(
                'About',
                'General information about the island and Contact info',
                '/about/card.jpg',
                route('about')
            ),
        ];

        return view('pages.home', ['cards' => $cards]);
    }

    public function attractions(){
        $info = [
            new info(
                'Museum of Taniti',
                'This local museum contains artifacts from the native peoples of the island of Taniti. With items dating back hundreds of years, the displays will be sure to wow visitors',
                '/blank.jpg'
            ),
            new info(
                'Waterfall Trail',
                'Join ours dedicateed guides on a wonderful hike through the rainforests. View breathtaking visages of the water falls and ride an exhilerating zipline back down',
                '/blank.jpg'
            ),
            new info(
                'Beach Fun',
                'Sprawling, white beaches await you when visiting Taniti. Be prepared to not want to return home after your stay',
                '/blank.jpg'
            )
        ];

        return view('pages.attractions', ['info' => $info]);
    }
    
    public function transportation(){
        $info = [
            new info(
                'Bus',
                'Offering affordable transport, these buses are a staple of transportation. Guided tours are available',
                '/transportation/bus.jpg'
            ),
            new info(
                'ATV',
                'For a more engaged experience, try out the rentable quads to explore the island',
                '/transportation/atv.jpg'
            )
        ];

        return view('pages.transportation', ['info' => $info]);
    }
    
    public function lodging(){
        $info = [
            new info(
                'Hotels',
                'Enjoy the variety of choice between many different hotels and hostels',
                '/blank.jpg'
            ),
            new info(
                'Resort',
                'For an enhanced experience during your stay, condsider the large 4 star resort home to Taniti',
                '/blank.jpg'
            ),
            new info(
                'Bed and Breakfast',
                'Enjoy the complimentary service of a bed and breakfast during your stay at one of many locations',
                '/blank.jpg'
            )
        ];

        return view('pages.lodging', ['info' => $info]);
    }
    
    public function about(){
        $abouts = [
            new about(
                'Taniti Island',
                'Taniti covers a bit less than 500 square miles of land in the Pacific ocean, represented by a population of 20,000<br>
                The economy is predominantly led by the agriculture and fishing industries, though tourism is becoming more popular<br>
                The island contains a plethora of environments, including lush rainforests, sandy beaches, and even an inactive volcano'
            ),
            new about('Emergency', '555-555-5555'),
            new about('General Information', '555-555-5555'),
            new about('Tour Scheduling', '555-555-5555'),
            new about('Resort Front Desk', '555-555-5555')
        ];

        return view('pages.about', ['abouts' => $abouts]);
    }
    
    public function faq(){
        $faqs = [
            new faqPair(
                'currency',
                'What currency is accepted?', 
                'Taniti\'s offical currency is the US Dollar, though other common currencies like yen and euros may be accepted at businesses'),
            new faqPair(
                'language',
                'What languages are spoken?', 
                'Most younger residents and city folk speak English, though rural and older members of the island may not'),
            new faqPair(
                'crime',
                'Should I be worried about crime?', 
                'Violent crime is very rare, though pickpocketing and the like will increase with tourism. Please keep track of your valuables to be safe'),
            new faqPair(
                'alcohol',
                'How is alcohol handled on the island?', 
                'Alcohol may be served to anyone above the age of 18, though not strictly enforced. No alcohol may be served or sold between 12:00am and 9:00am'),
            new faqPair(
                'power',
                'Do I need a power adapter for my devices?',
                'Taniti uses the 120 volt outlet, standard in the United States. If you are coming from other areas of the world, you may need an adapter to convert to the US outlet'
            )  
        ];
        return view('pages.faq', ['faqs' => $faqs]);
    }
}

class faqPair {
    private $name, $question, $answer = '';

    public function __construct($name, $question, $answer){
        $this->name = $name;
        $this->question = $question;
        $this->answer = $answer;
    }

    public function getName(){
        return $this->name;
    }

    public function getQuestion(){
        return $this->question;
    }
    public function getAnswer(){
        return $this->answer;
    }
}

class cardData {
    private $name, $description, $imagename, $link = '';

    public function __construct($name, $description, $imagename, $link){
        $this->name = $name;
        $this->description = $description;
        $this->imagename = $imagename;
        $this->link = $link;
    }

    public function getName(){
        return $this->name;
    }

    public function getDescription(){
        return $this->description;
    }

    public function getImagename(){
        return $this->imagename;
    }

    public function getLink(){
        return $this->link;
    }
}

class info {
    private $name, $description, $imagename = '';

    public function __construct($name, $description, $imagename){
        $this->name = $name;
        $this->description = $description;
        $this->imagename = $imagename;
    }

    public function getName(){
        return $this->name;
    }

    public function getDescription(){
        return $this->description;
    }

    public function getImagename(){
        return $this->imagename;
    }
}

class about {
    private $name, $description = '';

    public function __construct($name, $description){
        $this->name = $name;
        $this->description = $description;
    }

    public function getName(){
        return $this->name;
    }

    public function getDescription(){
        return $this->description;
    }
}